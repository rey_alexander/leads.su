import os
import time
import pytest

from selenium import webdriver
from selenium.webdriver.support.ui import Select

try:
	options = webdriver.ChromeOptions() 
	options.add_experimental_option("excludeSwitches", ["enable-logging"])
	driver = webdriver.Chrome()
	print('The session has started..')
	exec(open("logIn.py").read())
	exec(open("updateBio.py").read())
finally:
	print('The session has ended..')
	time.sleep(2)
	driver.quit()