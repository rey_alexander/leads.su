# Leads.su

**There is an auto-tests package for Leads pre-offer.**

- [ ] The first we need to run auto-tests is a Python 3.8+
You can download it on:
[Python.org](https://www.python.org/downloads/) _(please, **fill the checkbox "Add Python to PATH"**)_

- [ ] Also we need to check Python 3.8+ is in the PATH _(if it isn't you can to add it manually)_.

- [ ] The third we need is to create a new virtual environment _(if it hasn't created yet)_ through CMD _(you can select any directory you want to store in documents of our project)_:
`python -m venv reyalexander_env`

- [ ] Over it we need to install the libraries we'll work with in our project. The are in the 'requirements.txt' file, so we can continue in CMD like that:
`pip install -r requirements.txt`

- [ ] The fifth we need is to download and install the version of Chrome Driver that mathes our current Google Chrome version _(to have a compatibility two of them)_:
[Chrome Driver](https://sites.google.com/a/chromium.org/chromedriver/downloads)
To know the current Google Chrome version
[just visit this link via Google Chrome](chrome://version/)

- [ ] And finally we need to add Chrome Driver we've installed to the PATH _(we need to do it manually)_.


**So, we're ready to the first run of our Leads.su auto-test!**

We need:
1. To install all documents out of this git-repository, cause Main.py, updateBio.py and logIn.py scripts are associated with each other
2. To activate our virt. env. _(if it doesn't active, through CMD)_ by:
`reyalexander_env\Scripts\activate.bat`
3. To change current directory to the project directory _(also through CMD)_
4. And to run the Main.py script by command:
`python Main.py`

Here is a new Chrome tab that has opened automatically and it signals our test is executing.
Well done!

**P.S.**
If you have any questions or problems with auto-tests you can send me a 'HELP' message at _shinigami111sama@gmail.com_