driver.get("https://webmaster.leads.su/account/user/update")
print('Update has started...')
time.sleep(1)

nameField = driver.find_element_by_id("webmaster_modules_account_forms_user_UserUpdateForm_first_name")
nameField.clear()
nameField.click()
nameField.send_keys('Rey Alexander')
print("Name is sent")
	
surnameField = driver.find_element_by_id("webmaster_modules_account_forms_user_UserUpdateForm_last_name")
surnameField.clear()
surnameField.click()
surnameField.send_keys('Hernandez Dolgorukov')
print("Surname is sent")

preferableContactField = Select(driver.find_element_by_id("webmaster_modules_account_forms_user_UserUpdateForm_preferable_contact"))
preferableContactField.select_by_value('phone')
print("Preferable contact is chosen")
	
birthdayField = driver.find_element_by_id("webmaster_modules_account_forms_user_UserUpdateForm_birthday")
birthdayField.clear()
birthdayField.click()
birthdayField.send_keys('19990817')
print("Birthday is sent")

skypeField = driver.find_element_by_id("webmaster_modules_account_forms_user_UserUpdateForm_skype")
skypeField.clear()
skypeField.click()
skypeField.send_keys('live:shinigami111sama_1')
print("Skype is sent")

icqField = driver.find_element_by_id("webmaster_modules_account_forms_user_UserUpdateForm_icq")
icqField.clear()
icqField.click()
icqField.send_keys('excuseMe_idontHaveit')
print("ICQ is sent")

telegramField = driver.find_element_by_id("webmaster_modules_account_forms_user_UserUpdateForm_telegram")
telegramField.clear()
telegramField.click()
telegramField.send_keys('@Rey_Alexander')
print("Telegram is sent")

photoField = driver.find_element_by_id("webmaster_modules_account_forms_user_UserUpdateForm_photo")
photo_currentDir = os.path.abspath(os.path.dirname(__file__))
file_path = os.path.join(photo_currentDir, 'ReyAlexander1.jpg')
photoField.send_keys(file_path)
print("Photo is downloaded")

twoFAField = Select(driver.find_element_by_id("webmaster_modules_account_forms_user_UserUpdateForm_is_use_2fa"))
twoFAField.select_by_value("0")
print("2fa is turned")

submitButton = driver.find_element_by_css_selector("#save-btn.btn.blue")
driver.execute_script("return arguments[0].scrollIntoView(true);", submitButton)
submitButton.click()
time.sleep(4)

operationSuccess = driver.find_element_by_xpath("/html/body/div[2]/div[2]/div/div[1]/div").text
print(operationSuccess)
print('Updated has finished successfully')